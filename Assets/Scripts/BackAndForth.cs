﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackAndForth : MonoBehaviour {
    private float startX;
    public float amountToMove;
    public bool left;
    public float speed;
    // Use this for initialization
    void Start () {
		startX = gameObject.transform.position.x;
    }
	
	// Update is called once per frame
	void Update () {
        float x = gameObject.transform.position.x;
        float y = gameObject.transform.position.y;
        if (x < startX + amountToMove && !left)
        {
            gameObject.transform.position = new Vector2(x + speed, y);
        }
        else if (x >= startX + amountToMove && !left)
        {
            left = true;
        }
        else if (x >= startX && left)
        {
            gameObject.transform.position = new Vector2(x - speed, y);
        }
        else if (x < startX && left)
        {
            left = false;
        }
    }
}
