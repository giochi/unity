namespace sdeg
{
    public class Jump : IControllableDecorator
    {
        public Jump(float xVal, float yVal, IControllable toDecorate) : base(xVal, yVal, toDecorate) {
        }
        public override bool IsItToDo()
        {

            return Jump();
        }
    }
}
