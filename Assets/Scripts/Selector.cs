using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using System.Collections;

class ComparableByRowColPosition: IComparer
{
	public int Compare( object foll, object prec)
	{
		Vector2 follPos = ((Rigidbody2D)foll).position;
		Vector2 precPos = ((Rigidbody2D)prec).position;
		int yDist = (int)(follPos.y - precPos.y);
		return yDist != 0 ? yDist:((int)(follPos.x - precPos.x));
	}
}
public class Selector: MonoBehaviour, IComparable<MonoBehaviour>
{
	public int CompareTo(MonoBehaviour x)
	{
		return 0;
	}
	private Rigidbody2D[] bodies;
	private int curr;
	private string level;
	void Start()
	{
		bodies = FindObjectsOfType<Rigidbody2D>();
		Array.Sort( bodies, new ComparableByRowColPosition());
		Debug.Log("bodies.Length = " + bodies.Length);
		level = bodies[curr].name;
	}
	void Update()
	{
		int increment = (Input.GetKeyDown(KeyCode.RightArrow) ? 1 : 0) - (Input.GetKeyDown(KeyCode.LeftArrow) ? 1 : 0);
		if (0 != increment)
		{
			curr = (curr + increment + bodies.Length) % bodies.Length;
			Debug.Log("Input.GetKeyDown(KeyCode.RightArrow) = " + Input.GetKeyDown(KeyCode.RightArrow));
			Debug.Log("Input.GetKeyDown(KeyCode.LeftArrow) = " + Input.GetKeyDown(KeyCode.LeftArrow));
			gameObject.transform.position = bodies[curr].position;
		}else if( Input.GetKeyDown(KeyCode.Space))
		{
			if (level.Equals("RestartAllOverAgain"))
			{
				Debug.Log("Game to restart all over again!");
				SceneManager.LoadScene("Level 1");
			}
			else if (level.Equals("QuitTheGame"))
			{
				Debug.Log("Game to quit!");
				Application.Quit();
			}
			else
			{
				Debug.Log("Level 1 to load!");
				SceneManager.LoadScene(level);
			}
		}
	}
	void OnTriggerEnter2D( Collider2D other)
	{
		level = other.name;
		Debug.Log("level = <" + level + ">");
	}
}
