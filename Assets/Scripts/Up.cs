namespace sdeg
{
    public class Up : IControllableDecorator
    {
        public Up(float xVal, float yVal, IControllable toDecorate) : base(xVal, yVal, toDecorate) { }

        public override bool IsItToDo()
        {
            return Up();
        }
    }
}
