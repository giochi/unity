using UnityEngine;

class Swim: MonoBehaviour
{
	public AudioSource WaterSplash;

	void OnTriggerEnter2D( Collider2D other)
	{
		if( other.tag == "Player")
		{
			WaterSplash.Play();
		}
	}
}
