using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using sdeg;

public class Player : MonoBehaviour, IControllable {
    public Rigidbody2D rb;
    public float ySpeed = 2;
	public float jumpSpeed = 4;
	public float fallSpeed = 4;
	public bool onGround = false;
    public Transform bottom;
    public float radius = 0.1F;
    public LayerMask layer;
    public Animator anim;
    private bool facing;
    public int coins;
    public IControllableDecorator myController;
	public Touch Touch;

	public Vector2 StartPosition() { return new Vector2(-109.54F, 4.38F); }
	#region IControllable input methods

	public bool Right()
    {
        return Input.GetKey(KeyCode.RightArrow) || Touch.Right;
    }
    public bool Left()
    {
        return Input.GetKey(KeyCode.LeftArrow) || Touch.Left;
    }
    public bool Up()
    {
        return (Input.GetKey(KeyCode.UpArrow) || Touch.Up) && transform.position.y <= Environment.MaxY;
    }
    public bool Down()
    {
        return Input.GetKey(KeyCode.DownArrow) || Touch.Down;
    }
    public bool Jump()
    {
        return (Input.GetKey(KeyCode.Space) || Touch.Jump) && onGround;
    }
	#endregion

	public void Control( float xVel, float yVel)
    {
        if (xVel != 0 && yVel == 0)
        {
            rb.velocity = new Vector2(xVel, rb.velocity.y);
        }
        else if (xVel == 0 && yVel != 0)
        {
            rb.velocity = new Vector2(rb.velocity.x, yVel);
        }
        else if (xVel != 0 && yVel != 0)
        {
            rb.velocity = new Vector2(xVel, yVel);
        }
        if( xVel != 0)
        {
            anim.SetBool("Walking", onGround);
            if (facing != xVel < 0 && (facing = xVel < 0))
            {
                transform.localScale = new Vector3(-1f, 1f, 1f);
            }
            else if (facing != xVel > 0 && (facing = xVel > 0))
            {
                transform.localScale = new Vector3(1f, 1f, 1f);
            }
        }
        else
        {
            anim.SetBool("Walking", false);
        }
    }
    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        myController = new Jump( 0f, jumpSpeed
                        , new Up(0f, jumpSpeed
                        , new Down(0f, -fallSpeed
                        , new Right( ySpeed, 0f
                        , new Left(-ySpeed, 0f
                        , this)))));
	}

	// Update is called once per frame
	void Update () {
        myController.Control(0,0);
        /*
        if (Input.GetKey(KeyCode.LeftArrow) && !Input.GetKey(KeyCode.UpArrow))
        {
            if(!facing)
            {
                transform.localScale = new Vector3( -1f, 1f, 1f);
                facing = true;
            }
            rb.velocity = new Vector2(- 2, rb.velocity.y);
            anim.SetBool("Walking", true);
        }
        else if (Input.GetKey(KeyCode.RightArrow) && !Input.GetKey(KeyCode.UpArrow))
        {
            if ( facing)
            {
                transform.localScale = new Vector3(1f, 1f, 1f);
                facing = false;
            }
            rb.velocity = new Vector2(2, rb.velocity.y);
            anim.SetBool("Walking", true);
        }
        else if (Input.GetKey(KeyCode.Space) && onGround)
        {
            rb.velocity = new Vector2(rb.velocity.x, jumpSpeed);
            anim.SetBool("Walking", false);
        }
        else if (Input.GetKey(KeyCode.UpArrow))
        {
            anim.SetBool("Walking", true);
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                rb.velocity = new Vector2(-2, jumpSpeed);
            }
            else if (Input.GetKey(KeyCode.RightArrow))
            {
                rb.velocity = new Vector2(2, jumpSpeed);
            }
            else
            {
                rb.velocity = new Vector2(rb.velocity.x, jumpSpeed);
            }
        }
        else if (Input.GetKey(KeyCode.DownArrow))
        {
            rb.velocity = new Vector2(rb.velocity.x, -10 * jumpSpeed);
            anim.SetBool("Walking", true);
        }
        else
        {
            anim.SetBool("Walking", false);
        }
        */
    }
    void FixedUpdate(){
        onGround = Physics2D.OverlapCircle( bottom.position, radius, layer);
    }
}
