namespace sdeg
{
    public class Right: IControllableDecorator
    {
        public Right(float xVal, float yVal, IControllable toDecorate) : base(xVal, yVal, toDecorate)
        {
        }

        public override bool IsItToDo()
        {
            return Right();
        }
    }
}