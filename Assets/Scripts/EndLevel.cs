using UnityEngine;
using UnityEngine.SceneManagement;

class EndLevel: MonoBehaviour
{
	public string nextScene = "Level Select";
	void OnTriggerEnter2D( Collider2D other)
	{
		if( other.tag == "Player")
		{
			Application.Quit();
			SceneManager.LoadScene(nextScene);
		}
	}
}
