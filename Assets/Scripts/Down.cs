namespace sdeg
{
    public class Down : IControllableDecorator
    {
        public Down(float xVal, float yVal, IControllable toDecorate) : base(xVal, yVal, toDecorate) { }

        public override bool IsItToDo()
        {
            return Down();
        }
    }
}
