using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectCoin : MonoBehaviour {
	public GameObject Sparkle;
	public AudioSource bling;
	// Use this for initialization
	void Start () {
    }

    // Update is called once per frame
    void Update () {
    }
    
    private void OnTriggerEnter2D( Collider2D other)
    {
        if( other.tag == "Player")
        {
			bling.Play();
			other.GetComponent<Player>().coins++;
			Instantiate<GameObject>( Sparkle, gameObject.transform.position, gameObject.transform.rotation);
            Object.Destroy( gameObject);
        }
    }
}
