using UnityEngine;

public class Touch: MonoBehaviour
{
	public bool Down;
	public void DownPressed() { Down = true; }
	public void DownRelesed() { Down = false; }
	public bool Up;
	public void UpPressed() { Up = true; }
	public void UpRelesed() { Up = false; }
	public bool Left;
	public void LeftPressed() { Left = true; }
	public void LeftRelesed() { Left = false; }
	public bool Right;
	public void RightPressed() { Right = true; }
	public void RightRelesed() { Right = false; }
	public bool Jump;
	public void JumpPressed() { Jump = true; }
	public void JumpRelesed() { Jump = false; }
}
