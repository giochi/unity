namespace sdeg
{
    public class Left: IControllableDecorator
    {
        public Left(float xVal, float yVal, IControllable toDecorate) : base(xVal, yVal, toDecorate)
        {
        }
        public override bool IsItToDo()
        {
            return Left();
        }
    }
}