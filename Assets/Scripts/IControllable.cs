﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IControllable {
    void Control( float xVel, float yVel);
    bool Left();
    bool Right();
    bool Up();
    bool Down();
    bool Jump();
}
