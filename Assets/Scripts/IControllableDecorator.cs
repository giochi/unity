﻿public abstract class IControllableDecorator : IControllable
{
    internal protected IControllable toDecorate;
    public float XVal { get; set; }
    public float YVal { get; set; }

    public void Control(float xVel, float yVel)
    {
        if (IsItToDo())
        {
            toDecorate.Control(xVel + XVal, yVel + YVal);
        }
        else
        {
            toDecorate.Control(xVel, yVel);
        }
    }
    public bool Right()
    {
        return toDecorate.Right();
    }
    public bool Left()
    {
        return toDecorate.Left();
    }
    public bool Up()
    {
        return toDecorate.Up();
    }
    public bool Down()
    {
        return toDecorate.Down();
    }
    public bool Jump()
    {
        return toDecorate.Jump();
    }
    public abstract bool IsItToDo();
    public IControllableDecorator(float xVal, float yVal, IControllable toDecorate)
    {
        this.toDecorate = toDecorate;
        XVal = xVal;
        YVal = yVal;
    }
}
