﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace sdeg
{
    public abstract class Controller : Player
    {
        internal protected Player toDecorate;
        public Controller(Player toDecorate)
        {
            this.toDecorate = toDecorate;
        }
        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        internal protected abstract void Update();
        
    }
}