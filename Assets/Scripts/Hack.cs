﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;
using System;
using System.Reflection;

public class Hack : MonoBehaviour {
    Thread t;
    // Use this for initialization
    void Start () {
        t = new Thread(PrintNumbers);
        t.Name = "SergioThread";
        t.Start();
        t.Join();
        Debug.Log("class: " + this.GetType().FullName);
        Debug.Log("instanceId = " + GetInstanceID());
        Debug.Log("ToString = " + this);
        Debug.Log("name = " + name);
        Debug.Log("gameObject class: " + gameObject.GetType().FullName);
        Debug.Log("gameObject instanceId = " + gameObject.GetInstanceID());
        Debug.Log("gameObject ToString = " + gameObject);
        GameObject myGO = new GameObject("Sergio");
        myGO.transform.Translate( 1, 1, 1);
        Debug.Log("System.Enviroment.Version = " + System.Environment.Version);
        Type type = Type.GetType("Mono.Runtime");
        if (type != null)
        {
            MethodInfo displayName = type.GetMethod("GetDisplayName", BindingFlags.NonPublic | BindingFlags.Static);
            if (displayName != null)
                Debug.Log(displayName.Invoke(null, null));
        }
    }

    // Update is called once per frame
    void Update () {
   }
    static void PrintNumbers()
    {
        Thread th = Thread.CurrentThread;
        Thread.Sleep(1000);
        Debug.Log("Thread.CurrentThread.Name = " + th.Name);
        Debug.Log("Managed thread #: " + th.ManagedThreadId);
        Debug.Log("   Background thread: " + th.IsBackground);
        Debug.Log("   Thread pool thread: " + th.IsThreadPoolThread);
        Debug.Log("   Priority: " + th.Priority);
        Debug.Log("   Culture: " + th.CurrentCulture.Name);
        Debug.Log("   UI culture: " + th.CurrentUICulture.Name);
        for (int i = 0; i < 10; i++)
        {
            Debug.Log(i);
        }
    }
}
