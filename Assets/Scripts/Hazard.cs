using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hazard : MonoBehaviour {
    Player player;
    public GameObject Blood;
	public AudioSource Bomb;
	public GameObject restart;
	// Use this for initialization
	void Start () {
        player = FindObjectOfType<Player>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    private void OnTriggerEnter2D( Collider2D other)
    {
        if( other.tag == "Player")
        {
            StartCoroutine( respawnDelay());
        }
    }
    private IEnumerator respawnDelay()
    {
        Instantiate(Blood, player.transform.position, player.transform.rotation);
		Bomb.Play();
        player.enabled = false;
        player.GetComponent<Renderer>().enabled = false;
		player.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
		player.anim.SetBool("Walking", false);
        yield return new WaitForSeconds(1);
        player.transform.position = restart.transform.position;
        player.GetComponent<Renderer>().enabled = true;
        player.enabled = true;
    }
}
